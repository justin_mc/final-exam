﻿using System;

/// <summary>
/// code-behind for the main page
/// </summary>
/// <author>
/// Justin McConnell
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public partial class Default : System.Web.UI.Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// Handles the Click event of the btnGoFeedback control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGoFeedback_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerFeedback.aspx");
    }
    /// <summary>
    /// Handles the Click event of the btnGoCustomerList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGoCustomerList_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerList.aspx");
    }
}