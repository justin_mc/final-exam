﻿using System;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    /// <summary>
    /// Handles the SelectedIndexChanged event of the GridView1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.DetailsView1.PageIndex = this.GridView1.SelectedIndex;
    }
    protected void DetailsView1_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
    {

    }
    /// <summary>
    /// Handles the OnItemInserted event of the DetailsView1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DetailsViewInsertedEventArgs"/> instance containing the event data.</param>
    protected void DetailsView1_OnItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblOtherError.Text = "A database error has occured. " + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblOtherError.Text = "Another user may have updated the category. " + "Please try again.";
        }
    }

    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
}