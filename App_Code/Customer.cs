﻿using System;

/// <summary>
/// The Customer class
/// </summary>
/// /// <author>
/// Justin McConnell
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public class Customer

{
    private string _name;
    private string _address;
    private string _city;
    private string _state;
    private string _zipCode;
    private string _phone;
    private string _email;
    private string _customerId;

    public String GetName()
    {
        return this._name;
    }
    public String SetName(String name)
    {
        return this._name = name;
    }
    public String GetAddress()
    {
        return this._address;
    }
    public String SetAddress(String address)
    {
        return this._address = address;
    }
    public String GetCity()
    {
        return this._city;
    }
    public String SetCity(String city)
    {
        return this._city = city;
    }
    public String GetState()
    {
        return this._state;
    }
    public String SetState(String state)
    {
        return this._state = state;
    }
    public String GetZip()
    {
        return this._zipCode;
    }
    public String SetZip(String zip)
    {
        return this._zipCode = zip;
    }
    public String GetPhone()
    {
        return this._phone;
    }
    public String SetPhone(String phone)
    {
        return this._phone = phone;
    }
    public String GetEmail()
    {
        return this._email;
    }
    public String SetEmail(String email)
    {
        return this._email = email;
    }
    public String GetCustomerId()
    {
        return this._customerId;
    }
    public String SetCustomerId(String id)
    {
        return this._customerId = id;
    }
}