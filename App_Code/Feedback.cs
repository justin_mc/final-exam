﻿using System;

/// <summary>
/// The feedback class
/// </summary>
/// <author>
/// Justin Mcconnell
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public class Feedback
{
    private String _feedbackId;
    private String _customerId;
    private String _softwareId;
    private String _supportId;
    private String _dateOpened;
    private String _dateClosed;
    private String _title;
    private String _description;


	public Feedback(string feedbackId, string customerId, string softwareId, string supportId, string dateOpened, string dateClosed, string title, string description)
	{
	    this._feedbackId = feedbackId;
	    this._customerId = customerId;
	    this._softwareId = softwareId;
	    this._supportId = supportId;
	    this._dateOpened = dateOpened;
	    this._dateClosed = dateClosed;
	    this._title = title;
	    this._description = description;
	}

    public String GetFeedbackId()
    {
        return this._feedbackId;
    }

    public void SetFeedbackId(String feedbackId)
    {
        this._feedbackId = feedbackId;
    }
    public String GetCustomerId()
    {
        return this._customerId;
    }

    public void SetCustomerId(String customerId)
    {
        this._customerId = customerId;
    }
    public String GetSoftwareId()
    {
        return this._softwareId;
    }

    public void SetSoftwareId(String softwareId)
    {
        this._softwareId = softwareId;
    }
    public String GetSupportId()
    {
        return this._supportId;
    }

    public void SetSupportId(String supportId)
    {
        this._supportId = supportId;
    }
    public String GetDateOpened()
    {
        return this._dateOpened;
    }

    public void SetDateOpened(String dateOpened)
    {
        this._dateOpened = dateOpened;
    }
    public String GetDateClosed()
    {
        return this._dateClosed;
    }

    public void SetDateClosed(String dateClosed)
    {
        this._dateClosed = dateClosed;
    }
    public String GetTitle()
    {
        return this._title;
    }

    public void SetTitle(String title)
    {
        this._title = title;
    }
    public String GetDescription()
    {
        return this._description;
    }

    public void SetDescription(String description)
    {
        this._description = description;
    }

    public String FormatFeedback()
    {
        return "Feedback for software " + this.GetSoftwareId() + " closed " + this.GetDateClosed() + "(" +
               this.GetDescription() + ")";
    }
}