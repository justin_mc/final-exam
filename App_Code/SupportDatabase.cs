﻿using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data.OleDb;

/// <summary>
/// Summary description for SupportDatabase
/// </summary>
public class SupportDatabase
{
    /// <summary>
    /// Gets all support staff.
    /// </summary>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable GetAllSupportStaff()
    {
        OleDbConnection connect = new OleDbConnection(
            ConfigurationManager.ConnectionStrings["CustomerConnect"].ConnectionString);
        const string @select = "SELECT Name "
                               + "FROM Support";
        OleDbCommand command = new OleDbCommand(select, connect);
        connect.Open();
        OleDbDataReader reader = command.ExecuteReader();
        return reader;
    }
}