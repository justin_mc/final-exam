﻿
/// <summary>
/// Summary description for ListItem
/// </summary>
/// <author>
/// Justin McConnell
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public class ListItem
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ListItem"/> class.
    /// </summary>
    /// <param name="customer">The customer.</param>
	public ListItem(Customer customer)
	{
	    this.Customer = customer;
	}

    /// <summary>
    /// Gets or sets the customer.
    /// </summary>
    /// <value>
    /// The customer.
    /// </value>
    public Customer Customer { get; set; }

    /// <summary>
    /// Displays this instance.
    /// </summary>
    /// <returns></returns>
    public string Display()
    {
        string displayString = this.Customer.GetName() + ": " 
            + this.Customer.GetPhone() + "; " + this.Customer.GetEmail();
        return displayString;
    }
}