﻿using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data.OleDb;

/// <summary>
/// Summary description for FeedbackDatabase
/// </summary>
public class FeedbackDatabase
{
    /// <summary>
    /// Gets the open feedback incidents.
    /// </summary>
    /// <param name="supportStaffId">The support staff identifier.</param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable GetOpenFeedbackIncidents(int supportStaffId)
    {
        OleDbConnection connection = new OleDbConnection(
            ConfigurationManager.ConnectionStrings["CustomerConnect"].ConnectionString);
        const string @select = "SELECT SupportID, Name "
                               + "FROM Support "
                               + "WHERE SupportID IN "
                               + "(SELECT DISTINCT SupportID FROM Support "
                               + "WHERE SupportID IS NOT NULL) "
                               + "ORDER By Name ";

        OleDbCommand command = new OleDbCommand(select, connection);
        command.Parameters.AddWithValue("CategoryID", supportStaffId);
        connection.Open();
        OleDbDataReader reader = command.ExecuteReader();
        return reader;
    }
}