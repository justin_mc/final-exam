﻿using System;

/// <summary>
/// The list of contacts.
/// </summary>
/// <author>
/// Justin McConnell
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public partial class ContactList : System.Web.UI.Page
{
    private CustomerList _contacts;
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this._contacts = CustomerList.GetList();

        if (!IsPostBack)
        {
            this.DisplayCustomers();
        }
    }

    /// <summary>
    /// Displays the customers.
    /// </summary>
    private void DisplayCustomers()
    {
        this.lstbxContacts.Items.Clear();

        for (int i = 0; i < this._contacts.Count; i++)
        {
            ListItem item = this._contacts[i];
            this.lstbxContacts.Items.Add(item.Display());
        }
    }
    /// <summary>
    /// Handles the Click event of the btnSelectAdditionalCustomers control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnSelectAdditionalCustomers_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerList.aspx");
    }
    /// <summary>
    /// Removes the selected customer.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRemoveCustomer_Click(object sender, EventArgs e)
    {
        if (this._contacts.Count > 0)
        {
            if (this.lstbxContacts.SelectedIndex != -1)
            {
                this._contacts.RemoveAt(this.lstbxContacts.SelectedIndex);
                this.DisplayCustomers();
            }
            else
            {
                this.lblMessages.Text = "A contact must be selected.";
            }
        }
    }
    /// <summary>
    /// Handles the SelectedIndexChanged event of the lstbxContacts control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void lstbxContacts_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.lblMessages.Text = "";
    }
    /// <summary>
    /// Clears the contacts list when clicked.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnClear_Click(object sender, EventArgs e)
    {
        this._contacts.Clear();
        this.DisplayCustomers();
    }
}